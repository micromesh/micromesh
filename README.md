# Micromesh

µmesh is a stationless mesh networking system for zero-infrastructure networking.

## Principals

  * Zero-infrastructure — nodes form a µmesh, communicating directly, without any stations or other infrastructure.
  * Secure — data is always encrypted, key exchange is secure (DH, etc), asymmetric keys for identity verification.
  * Hardened — data is modulated with error correction and orthogonal modulation using random codes (keys).
  * Zero-trust — other nodes are inherently distrusted.

## Implementation

Snickerdoodle, pakparse, zynq channels. RF of some sort. CMDA. Maybe OFDM in the future.

Data and control bands. Control band for establishing comms via exchanging CDMA keys.

## Configuration and Network

Being zero-infrastructure, µmeshes do not have fixed infrastructure with centralized configuration. Thus, µmesh must consider configuration sharing and management.

A configuration zone is a collection of µmesh nodes that agree they are part of a zone. Membership in a configuration zone entails shared encryption keys. Intra-zone communication is encrypted with these keys to ensure that only members of the zone can access these communications. As well, local zones are configuration zones where the devices agree that all zone traffic should only be communicated directly to another member of the zone.

Configuration control and encryption keys could be associated with certificates. So the entity with control over the associated private key is the zone administrator and can control the zone via signed configuration manifests. Thus, nodes can freely share configuration updates without trusting each other.

## Transfers

Energy credit cryptocurrency. Data transfer cost is base cost/overhead + cost per bit. Generally, things should balance out. Credit transfers are a transfer record signed by both parties. Long term balances are verified via cryptocurrency magic. Short term balances are unverified to a certain extent.

When a node is first turned on, it tells the network that its alive. After that point, it starts accumulating credits at a slow rate. Hoarded credits eventually expire, to place a limit on accumulation. If you run out of credits, you'll have to buy them from someone. If credits didn't naturally accumulate, where would they come from? Maybe each transfer creates a few extra credits.

Node A declares that it wishes to transmit a packet to Node B. Routing magic happens. A decides that it will transmit via X to Y to B. A signs the packet and transmits it to X. X signs the packet and sends the receipt to A. X transmits to Y, which sends a receipt to A. Y transmits to B, which sends a receipt to A. A signs the various receipts, and posts a transaction crediting X, Y, and B.

If a given node consistently fails to pay, other nodes will refuse to accept transfers from it. Being a bad actor won't get you very far.

Via a special message, Node A can declare that it will pay for Node B to send a packet to A. In this way, if Node A is a browser and Node B is an HTTP, A pays for the request and the response.

Maybe, Node A says "I want to transfer a packet to B". X says "I can get your packet to Y with latency τ". Y, Z, etc say similar. A says, "I will pay for the packet to go from me to X to Y to B".

Perhaps recipient nodes simply post the verification, which debits from the sender. And maybe if the transfer time is too long, the debit fails?

Variable thresholds for acceptable latencies etc to allow a balance between security and convenience?
